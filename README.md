# monnef's Dalamud plugin repository

## Installation (adding custom plugin repository)

1) Open `Dalamud Settings` (e.g. escape to show `System` menu and there `Dalamud Settings`, or from `Plugin Installer` by clicking on the `Settings` button at the bottom, or via `/xlsettings`)
2) Switch to `Experimental` tab
3) Find `Custom Plugin Repositories` section
4) paste following URL
    ```
    https://gitlab.com/monnef/dalamud-plugin-repo/-/raw/master/master.json
    ```
   into empty text input
5) click on `+`
6) click on save icon 💾 in bottom right corner
7) now you should see in plugin installer all plugins from this repository (you still have to install them from there)

## Plugins available
* [Teiden](https://gitlab.com/monnef/teiden)
* [Venture Master](https://gitlab.com/monnef/venture-master)
* [Mayai](https://gitlab.com/monnef/mayai)
